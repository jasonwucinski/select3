﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class SettingsDialog : MonoBehaviour {
    public GameObject screenParent;
    public HUD hud;
    public GameObject playButton;
    public AudioSource menuButtonClickClip;
    public AudioSource menuUpClip;
    public AudioSource menuDownClip;
    public Toggle cbMute;
    
    // Use this for initialization
    void Start () {
        screenParent.SetActive(false);
        int m = PlayerPrefs.GetInt("isMute");
        if(m == 0)
        {           
            cbMute.isOn = false;
        }
        else
        {           
            cbMute.isOn = true;
        }


    }
	
	// Update is called once per frame
	void Update () {
		
	}
    public void ShowSettingsPress()
    {
        screenParent.SetActive(true);
        if (hud.isMute == false)
        {
            menuButtonClickClip.Play();
        }
        
        Animator animator = GetComponent<Animator>();

        if (animator)
        {
            animator.Play("SettingsDialogShow");
        }
    }

    public void HideSettingsPress(string levelName)
    {              
        Animator animator = GetComponent<Animator>();
       
        if (cbMute.isOn == true) {           
            hud.isMute = true;
            PlayerPrefs.SetInt("isMute", 1);
        }
        else
        {
            menuButtonClickClip.Play();
            hud.isMute = false;
            PlayerPrefs.SetInt("isMute", 0);
        }
        
        if (animator)
        {
            animator.Play("SettingsDialogHide");
            
        }

        hud.OnGameUnPaused();     

    }

}
