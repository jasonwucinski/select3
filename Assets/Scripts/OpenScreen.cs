﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class OpenScreen : MonoBehaviour {
    public GameObject screenParent;
    public Text userName;

    // Use this for initialization
    List<string> perms = new List<string>() { "public_profile", "email", "user_friends" };

    void Start () {        
        screenParent.SetActive(false);
	}
    private void Awake()
    {

        //if (!FB.IsInitialized)
        //{
        //    if (!FB.IsInitialized)
        //    {
        //        // Initialize the Facebook SDK
        //        FB.Init(InitCallback, OnHideUnity);
        //    }
        //    else
        //    {
        //        // Already initialized, signal an app activation App Event
        //        FB.ActivateApp();
        //    }
        //}
    }

    private void InitCallback()
    {
        //if (FB.IsInitialized)
        //{
        //    // Signal an app activation App Event
        //    FB.ActivateApp();
        //    // Continue with Facebook SDK
        //    // ...
        //}
        //else
        //{
        //    Debug.Log("Failed to Initialize the Facebook SDK");
        //}
    }

    private void OnHideUnity(bool isGameShown)
    {
        if (!isGameShown)
        {
            // Pause the game - we will need to hide
            Time.timeScale = 0;
        }
        else
        {
            // Resume the game - we're getting focus again
            Time.timeScale = 1;
        }
    }

    // Update is called once per frame
    void Update () {
		
	}

    public void ArcadeModeClick()
    {
        SceneManager.LoadScene("LevelSelect");
    }
    public void MultiPlayerModeClick()
    {
        screenParent.SetActive(true);
        Animator animator = GetComponent<Animator>();       

        if (animator)
        {
            animator.Play("multiPlayerShow");
        }
    }

    public void TimedModeClick()
    {

    }
    public void RateButtonClick()
    {

    }
    public void SettingsButtonClick()
    {

    }
    public void HelpButtonClick()
    {

    }
}
