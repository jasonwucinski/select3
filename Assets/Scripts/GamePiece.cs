﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GamePiece : MonoBehaviour {

    public int selectedTilePosition;
    private int x;
    private int y;
    private Grid.PieceType type;
    private Grid grid;
    private MoveablePiece moveableComponent;
    private TilePiece tilePiece;
    public string TileColor, TileShape, TileFill, TileBG;
    public bool isSelected = false;
    public ClearablePiece clearableComponent;
    private SpriteRenderer Renderer;
    private GameObject HighlightGameObject;
    public Sprite HighlightSprite;
    public int Score;
    public int X
    {
        get { return x; }
        set {
            if (IsMoveable())
            {
                x = value;
            }
        }
    }
    public int Y
    {
        get { return y; }
        set
        {
            if (IsMoveable())
            {
                y = value;
            }
        }
    }
    public Grid.PieceType Type
    {
        get { return type; }
    }
    public Grid GridRef
    {
        get { return grid; }
    }
    public MoveablePiece MoveableComponent
    {
        get { return moveableComponent;  }
    }
    public TilePiece TilePiece
    {
        get { return tilePiece; }
    }

    // Use this for initialization
    void Start () {
		
	}

    void Awake()
    {
        moveableComponent = GetComponent<MoveablePiece>();
        tilePiece = GetComponent<TilePiece>();
        clearableComponent = GetComponent<ClearablePiece>();
        

    }
    // Update is called once per frame
    void Update () {
		
	}
    public void SetHighlight()
    {
        Vector3 position = new Vector3(X, Y, 1);
        Vector3 localScale = new Vector3(.5f, .5f);
        HighlightGameObject = new GameObject();
        Renderer = HighlightGameObject.AddComponent<SpriteRenderer>();

        HighlightGameObject.transform.position = tilePiece.transform.position;
        Renderer.sprite = HighlightSprite;
        Renderer.sortingLayerName = "highlight";
        HighlightGameObject.transform.localScale = localScale;

    }
    public void RemoveHighlight()
    {
        Destroy(HighlightGameObject);
    }

    public bool IsMoveable()
    {
        return moveableComponent != null;
    }
    public bool IsColored()
    {
        return tilePiece != null;
    }

    public void Init(int _x, int _y, Grid _grid, Grid.PieceType _type)
    {
        x = _x;
        y = _y;
        grid = _grid;
        type = _type;

    }
    void OnMouseDown()
    {
        grid.PressPiece(this);
       
    }
    void OnMouseUp()
    {
      

    }
    void OnMouseEnter()
    {

    }
    public bool IsClearable()
    {
        return clearableComponent != null;
    }
}
