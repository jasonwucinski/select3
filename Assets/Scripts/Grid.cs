﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Collections.Generic;
using Assets.Scripts;
using System.Linq;

public class Grid : MonoBehaviour {
    public int xDim;
    public int yDim;
    public int Score;
    public int ScoreModifier;
    public float FillTime;
    public bool inverse = false;    
    public bool gameOver;
    public Level level;
    public PiecePosition[] initialPieces;
    public PiecePrefab[] piecePrefabs;
    public GameObject backgroundPrefab;
    public int numberMovesLeft;
    public bool isFilling = false;
    public List<List<GamePiece>> RemainingMatches = new List<List<GamePiece>>();
    private Dictionary<PieceType, GameObject> piecePrefabDict;
    private GamePiece[,] pieces;
    public bool gamePaused;
    public AudioSource tileSelectClip;
    public AudioSource tileUnselectClip;
    public float tileSpace;



    private GameObject[,] bgPieces;
    
    [System.Serializable]
    public struct PiecePosition
    {
        public PieceType type;
        public int x;
        public int y;
    }
    public enum PieceType
    {
        NORMAL,
        COUNT,
        EMPTY,
        OBSTACLE,
        RANDOM

    };  

    [System.Serializable]
    public struct PiecePrefab
    {
        public PieceType type;
        public GameObject prefab;        
    };
    
	// Use this for initialization
	void Awake () {
        piecePrefabDict = new Dictionary<PieceType, GameObject>();
        for (int i = 0; i < piecePrefabs.Length; i++)
        {
            if (!piecePrefabDict.ContainsKey(piecePrefabs[i].type))
            {
                piecePrefabDict.Add(piecePrefabs[i].type, piecePrefabs[i].prefab);
            }
        }

        bgPieces = new GameObject[xDim, yDim];
        for (int x = 0; x < xDim; x++)
        {
            for (int y = 0; y < yDim; y++)
            {
                GameObject background = (GameObject)Instantiate(backgroundPrefab, GetWorldPosition(x,y), Quaternion.identity);           

                bgPieces[x, y] = background;
                background.transform.parent = transform;

            }
        }
        pieces = new GamePiece[xDim, yDim];
        for(int i=0; i< initialPieces.Length; i++)
        {
            if(initialPieces[i].x >=0 && initialPieces[i].x < xDim && initialPieces[i].y >=0 && initialPieces[i].y < yDim)
            {
                SpawnNewPiece(initialPieces[i].x, initialPieces[i].y, initialPieces[i].type);
            }
            
        }

        for (int x = 0; x < xDim; x++)
        {
            for (int y = 0; y < yDim; y++)
            {
                if(pieces[x,y] == null)
                {
                    SpawnNewPiece(x, y, PieceType.EMPTY);
                }               

            }
        }



        StartCoroutine(Fill());
    }
	
	// Update is called once per frame
	void Update () {

    
    }
    public int NumberOfPiecesRemaining(PieceType type)
    {
        int count = 0;
        for (int x = 0; x < xDim; x++)
        {
            for (int y = 0; y < yDim; y++)
            {
                if (pieces[x, y].Type == type)
                {
                    count++;
                }
            }
        }
        return count;
    }
    
   

    public List<GamePiece> GetConvertArrayToList()
    {
        List<GamePiece> converted = new List<GamePiece>();
        foreach(GamePiece piece in pieces) { 
            if(piece.Type == PieceType.NORMAL)
            {
                converted.Add(piece);
            }            
        }

        return converted;

    }





    private void ClearObstacles(int x, int y)
    {

        for(int adjacentX = x - 1; adjacentX <= x + 1; adjacentX++){
            if(adjacentX != x && adjacentX >=0 && adjacentX < xDim)
            {
                if(pieces[adjacentX,y].Type == PieceType.OBSTACLE && pieces[adjacentX, y].IsClearable())
                {
                    pieces[adjacentX, y].clearableComponent.Clear();
                    SpawnNewPiece(adjacentX, y, PieceType.RANDOM);
                }
            }
        }
        for (int adjacentY = y - 1; adjacentY <= y + 1; adjacentY++)
        {
            if (adjacentY != y && adjacentY >= 0 && adjacentY < yDim)
            {
                if (pieces[x, adjacentY].Type == PieceType.OBSTACLE && pieces[x, adjacentY].IsClearable())
                {
                    pieces[x, adjacentY].clearableComponent.Clear();
                    SpawnNewPiece(x, adjacentY, PieceType.RANDOM );
                }
            }
        }

    }



    public Vector2 GetWorldPosition(int x, int y)
    {     
        
        float j = (transform.position.y + yDim / 3.5f - y) + (y * tileSpace);
        float i = (transform.position.x - xDim / 3.50f + x) - (x * tileSpace);

        return new Vector2(i, j);
    }
    public GamePiece SpawnNewPiece(int x, int y, PieceType type)
    {
        Vector2 v2 = GetWorldPosition(x, y);
        GameObject newPiece = (GameObject) Instantiate(piecePrefabDict[type],v2 , Quaternion.identity);
        newPiece.transform.parent = transform;
        pieces[x, y] = newPiece.GetComponent<GamePiece>();
        pieces[x, y].Init(x, y, this, type);
   
        return pieces[x, y];
    }

    public IEnumerator Fill()
    {
        isFilling = true;
        yield return new WaitForSeconds(FillTime);
        yield return new WaitForSeconds(FillTime);
        yield return new WaitForSeconds(FillTime);
        yield return new WaitForSeconds(FillTime);
        yield return new WaitForSeconds(FillTime);
        yield return new WaitForSeconds(FillTime);
        yield return new WaitForSeconds(FillTime);


        while (FillStep())
        {
            inverse = !inverse;
            yield return new WaitForSeconds(FillTime);
        }
        isFilling = false;
    }
    public bool FillStep()
    {
        bool movedPiece = false;

        for(int y = yDim-2; y>=0; y--)
        {
            for (int loopX = 0; loopX < xDim; loopX++)
            {
                int x = loopX;
                if (inverse)
                {
                    x = xDim - 1 - loopX;
                }
                GamePiece piece = pieces[x, y];
                if (piece.IsMoveable())
                {
                    GamePiece pieceBelow = pieces[x, y + 1];
                    if(pieceBelow.Type == PieceType.EMPTY)
                    {
                        Destroy(pieceBelow.gameObject);
                        piece.MoveableComponent.Move(x, y + 1, FillTime);
                        
                        pieces[x, y + 1] = piece;                 
                        SpawnNewPiece(x, y, PieceType.EMPTY);
                        movedPiece = true;
                    }
                    else
                    {
                        for (int diag = -1; diag <= 1; diag++)
                        {
                            if (diag != 0)
                            {
                                int diagX = x + diag;

                                if (inverse)
                                {
                                    diagX = x - diag;
                                }

                                if (diagX >= 0 && diagX < xDim)
                                {
                                    GamePiece diagonalPiece = pieces[diagX, y + 1];

                                    if (diagonalPiece.Type == PieceType.EMPTY)
                                    {
                                        bool hasPieceAbove = true;

                                        for (int aboveY = y; aboveY >= 0; aboveY--)
                                        {
                                            GamePiece pieceAbove = pieces[diagX, aboveY];

                                            if (pieceAbove.IsMoveable())
                                            {
                                                break;
                                            }
                                            else if (!pieceAbove.IsMoveable() && pieceAbove.Type != PieceType.EMPTY)
                                            {
                                                hasPieceAbove = false;
                                                break;
                                            }
                                        }

                                        if (!hasPieceAbove)
                                        {
                                            Destroy(diagonalPiece.gameObject);
                                            piece.MoveableComponent.Move(diagX, y + 1, FillTime);
                                            pieces[diagX, y + 1] = piece;                                           
                                            SpawnNewPiece(x, y, PieceType.EMPTY);                                           
                                            movedPiece = true;
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                    }

                }
            }
        }
        for (int x = 0; x < xDim; x++)
        {
            GamePiece pieceBelow = pieces[x, 0];
            if(pieceBelow.Type == PieceType.EMPTY)
            {
                Destroy(pieceBelow.gameObject);
                GameObject newPiece = (GameObject)Instantiate(piecePrefabDict[PieceType.NORMAL], GetWorldPosition(x, -1), Quaternion.identity);
                newPiece.transform.parent = transform;                

                pieces[x, 0] = newPiece.GetComponent<GamePiece>();
                pieces[x, 0].Init(x, -1, this, PieceType.NORMAL);
                pieces[x, 0].MoveableComponent.Move(x, 0, FillTime);
                TilePiece.TileType tilePieceType = (TilePiece.TileType)Random.Range(0, pieces[x, 0].TilePiece.NumTiles);
                pieces[x, 0].TilePiece.SetTile(tilePieceType);
                TileProperties TileProperties = SetTileFeatures(tilePieceType);
                pieces[x, 0].TileColor = TileProperties.TileColor;
                pieces[x, 0].TileFill = TileProperties.TileFill;
                pieces[x, 0].TileShape = TileProperties.TileShape;
                pieces[x, 0].TileBG = TileProperties.TileBG;             

                movedPiece = true;
            }
           
        }

        
            return movedPiece;
    }
   

    private TileProperties SetTileFeatures(TilePiece.TileType tileType)
    {
       TileProperties TileProperties = new Assets.Scripts.TileProperties();
        string tileName = tileType.ToString();
        string[] words = tileName.Split('_');

        TileProperties.TileShape = words[0];
        TileProperties.TileBG = words[1];
        TileProperties.TileFill = words[2];
        TileProperties.TileColor = words[3];

        return TileProperties;
    }




    public void PressPiece(GamePiece piece)
    {
        if(gameOver == false && gamePaused == false)
        {
            level.OnSelectPiece(piece);
        }
         
    }   

    public bool ClearPiece(int x, int y)
    {
        if(pieces[x, y].clearableComponent.isBeingCleared)
        {
            pieces[x, y].clearableComponent.Clear();
            SpawnNewPiece(x, y, PieceType.EMPTY);
            ClearObstacles(x, y);
            return true;
        }       
        return false;
        
    }
    public void GameUnPause()
    {
        gamePaused = false;
    }
    public void GamePause()
    {
        gamePaused = true;
    }
    public void GameOver()
    {
        gameOver = true;
    }

}
