﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
public class LevelSelect : MonoBehaviour {

    [System.Serializable]
    public struct ButtonPlayerPrefs
    {
        public GameObject gameObject;
        public string playerPrefKey;
    }
    public ButtonPlayerPrefs[] buttons;
    public Image disabledBg;

	// Use this for initialization
	void Start () {
		for (int i =0; i< buttons.Length; i++)
        {
            int score = PlayerPrefs.GetInt(buttons[i].playerPrefKey, 0);
            for(int starIdx = 1; starIdx<=3; starIdx++)
            {
                Transform star = buttons[i].gameObject.transform.Find("star_" + starIdx);
                Transform starDisabled = buttons[i].gameObject.transform.Find("star_" + starIdx + "_disabled");
                Transform disabledBg = buttons[i].gameObject.transform.Find("disabled_bg");
                Transform levelNumber = buttons[i].gameObject.transform.Find("level_number");
                if (score <= 0)
                {                   
                    disabledBg.gameObject.SetActive(true);

                }
                else
                {
                    levelNumber.GetComponent<Text>().text = (i+1).ToString();
                    disabledBg.gameObject.SetActive(false);
                    if (starIdx <= score)
                    {
                        star.gameObject.SetActive(true);
                        starDisabled.gameObject.SetActive(false);
                    }
                    else
                    {
                        star.gameObject.SetActive(false);
                        starDisabled.gameObject.SetActive(true);
                    }
                }

                
            }        


        }
	}
	
	// Update is called once per frame
	void Update () {
		
	}



    public void OnButtonPress(string levelName)
    {
        SceneManager.LoadScene(levelName);

    }

}
