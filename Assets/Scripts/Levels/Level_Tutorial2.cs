﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Level_Tutorial2 : Level {
    public int numMoves;
    public int targetScore;
    public int timeInSeconds;
    private float timer;
    private bool timeOut;
    public int TargetNumDifferentBGFound;



    // Use this for initialization
      new void Start () {
        type = LevelType.TUTORIAL;
        hud.SetLevelType(type);    
        hud.SetTarget(TargetNumDifferentBGFound);
        hud.SetRemaingSubText("Time Remaining");
        hud.SetTargetSubText("Sets Remaining");
        hud.SetRemaining(string.Format("{00}:{1:00}", timeInSeconds/60, timeInSeconds %60) );
        GamePaused();     

        welcomeDialog.ShowWelcomePress("Now, Select " + TargetNumDifferentBGFound +  " Sets of Tiles, With Different Background Colors, In The Time Remaining!", "startGame");

        base.Start();
    }


    // Update is called once per frame
    void Update () {
        hud.SetTarget(TargetNumDifferentBGFound - NumSameBGFound);

        if (!timeOut && grid.gamePaused == false)
        {
            timer += Time.deltaTime;
            hud.SetRemaining(string.Format("{00}:{1:00}", (int)Mathf.Max((timeInSeconds - timer) / 60, 0), (int)Mathf.Max((timeInSeconds - timer) % 60), 0));
            if (timeInSeconds - timer <= 0)
            {
                GameLose();
                hud.SetGameOverText("Try Again!");
                timeOut = true;
            }
            else
            {
                if (NumSameBGFound >= TargetNumDifferentBGFound)
                {
                    hud.SetGameOverText("Great Job.  But these aren't a complete set, yet!");
                    GameWin();
                    timeOut = true;
                }
            }

        }
    }

    public override bool CheckForSet(GamePiece piece1, GamePiece piece2, GamePiece piece3, bool countPieces)
    {   
        bool isBGSet;
        int TempNumDifferentBGFound = 0;         


        if (piece1.TileBG != piece2.TileBG && piece2.TileBG != piece3.TileBG && piece1.TileBG != piece3.TileBG)
        {
            isBGSet = true;
            TempNumDifferentBGFound++;
        }        
        else
        {
            isBGSet = false;
        }


        if (isBGSet == true)
        {
            if (countPieces == true)
            {                
                if (TempNumDifferentBGFound > 0)
                {
                    NumSameBGFound++;
                }
                else
                {
                    NumDifferentBGFound++;
                }
            }

            return true;
        }
        else
        {
            return false;
        }


    }

}
