﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Level_Tutorial4 : Level {
    public int numMoves;
    public int targetScore;
    public int timeInSeconds;
    private float timer;
    private bool timeOut;
    public int TargetNumberSets;
    private int numSetsFound = 0;


    // Use this for initialization
    new void Start () {
        type = LevelType.TUTORIAL;
        hud.SetLevelType(type);     
        hud.SetTarget(TargetNumberSets);
        hud.SetRemaingSubText("Time Remaining");
        hud.SetTargetSubText("Sets Remaining");
        hud.SetRemaining(string.Format("{00}:{1:00}", timeInSeconds/60, timeInSeconds %60) );
        GamePaused();     

        welcomeDialog.ShowWelcomePress("Now, Select " + TargetNumberSets +  " Sets of Tiles, With The Different Shapes, Different Background Colors, and same Shape Colors, In The Time Remaining!", "startGame");
        base.Start();
    }

    // Update is called once per frame
    void Update () {
        hud.SetTarget(TargetNumberSets - numSetsFound);

        if (!timeOut && grid.gamePaused == false)
        {
            timer += Time.deltaTime;
            hud.SetRemaining(string.Format("{00}:{1:00}", (int)Mathf.Max((timeInSeconds - timer) / 60, 0), (int)Mathf.Max((timeInSeconds - timer) % 60), 0));
            if (timeInSeconds - timer <= 0)
            {
                GameLose();
                hud.SetGameOverText("Try Again!");
                timeOut = true;
            }
            else
            {
                if (numSetsFound >= TargetNumberSets)
                {
                    hud.SetGameOverText("Great Job.  But these aren't a complete set, yet!");
                    GameWin();
                    timeOut = true;
                }
            }

        }
    }
    public override int NumberOfSetsRemaining()
    {
        List<List<GamePiece>> NewList = new List<List<GamePiece>>();

        int count = 0;
        List<GamePiece> list = grid.GetConvertArrayToList();
        var result = GetPermutations(list, 3);
        foreach (var perm in result)
        {
            List<GamePiece> p = new List<GamePiece>();
            foreach (var c in perm)
            {
                p.Add(c);
            }
            NewList.Add(p);
        }

        foreach (List<GamePiece> pieces in NewList)
        {
            grid.RemainingMatches = new List<List<GamePiece>>();
            bool isSet = CheckForSet(pieces[0], pieces[1], pieces[2], false);
            if (isSet == true)
            {
                if (DoesPieceExist(grid.RemainingMatches, pieces) == false)
                {
                    grid.RemainingMatches.Add(pieces);
                    count++;
                }

            }
        }
        return count;
    }

    public override bool CheckForSet(GamePiece piece1, GamePiece piece2, GamePiece piece3, bool countPieces)
    {      

        if (piece1.TileShape != piece2.TileShape && piece2.TileShape != piece3.TileShape && piece1.TileShape != piece3.TileShape)
        {
           
        }        
        else
        {
            return false;
        }

        if (piece1.TileBG != piece2.TileBG && piece2.TileBG != piece3.TileBG && piece1.TileBG != piece3.TileBG)
        {
            
        }
        else
        {
            return false;
        }

        if (piece1.TileColor == piece2.TileColor && piece2.TileColor == piece3.TileColor && piece1.TileColor == piece3.TileColor)
        {
        
        }
        else
        {
            return false;
        }

        return true;


    }

}
