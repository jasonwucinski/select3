﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Level_5 : Level
{
    public int numMoves;
    public int targetScore;
    public int timeInSeconds;
    private float timer;
    private bool timeOut;
    public int TargetNumberSets;
    private int numSetsFound = 0;


    // Use this for initialization
    new void Start()
    {
        type = LevelType.TIMER;
        hud.SetLevelType(type);
        hud.SetTarget(TargetNumberSets);
        hud.SetRemaingSubText("Time Remaining");
        hud.SetTargetSubText("Sets Remaining");
        hud.SetRemaining(string.Format("{00}:{1:00}", timeInSeconds / 60, timeInSeconds % 60));
        GamePaused();

        welcomeDialog.ShowWelcomePress("Now, Select " + TargetNumberSets + " Sets in The Time Remaining!", "startGame");
        base.Start();
    }


    //public override bool CheckForSet(GamePiece piece1, GamePiece piece2, GamePiece piece3, bool countPieces)
    //{

    //    base.CheckForSet();


    //}

    // Update is called once per frame
    void Update()
    {
        hud.SetTarget(TargetNumberSets - numSetsFound);

        if (!timeOut && grid.gamePaused == false)
        {
            timer += Time.deltaTime;
            hud.SetRemaining(string.Format("{00}:{1:00}", (int)Mathf.Max((timeInSeconds - timer) / 60, 0), (int)Mathf.Max((timeInSeconds - timer) % 60), 0));
            if (timeInSeconds - timer <= 0)
            {
                GameLose();
                hud.SetGameOverText("Try Again!");
                timeOut = true;
            }
            else
            {
                if (numSetsFound >= TargetNumberSets)
                {
                    hud.SetGameOverText("Great Job.");
                    GameWin();
                    timeOut = true;
                }
            }

        }
    }

}
   
