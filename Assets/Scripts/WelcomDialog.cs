﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class WelcomDialog : MonoBehaviour {
    public GameObject screenParent;
    public Text directionsText;
    public HUD hud;
    public GameObject playButton;
    public GameObject menuButton;
    public GameObject restartButton;
    public string instructions;
    public AudioSource menuButtonClickClip;
    public AudioSource menuUpClip;
    public AudioSource menuDownClip;


    // Use this for initialization
    void Start () {
        screenParent.SetActive(true);        
    }
	
	// Update is called once per frame
	void Update () {
        
    }
    public void ShowWelcomePress(string welcomeText, string menuType)
    {
        screenParent.SetActive(true);
        
        if (menuType == "startGame")
        {
            instructions = welcomeText;
            directionsText.text = welcomeText;
            playButton.SetActive(true);
            menuButton.SetActive(false);
            restartButton.SetActive(false);
        }else if(menuType == "pauseGame")
        {
            if(hud.isMute== false)
            {
                menuButtonClickClip.Play();
            }
            
            directionsText.text = welcomeText + " " + instructions;
            playButton.SetActive(true);
            menuButton.SetActive(true);
            restartButton.SetActive(true);
        }       
        
        Animator animator = GetComponent<Animator>();

        if (animator)
        {
            animator.Play("WelcomeDialogShow");
        }
    }

    public void HideWelcomePress(string levelName)
    {              
        Animator animator = GetComponent<Animator>();
        if(hud.isMute == false)
        {
            menuButtonClickClip.Play();
        }
        
        if (animator)
        {
            animator.Play("WelcomeDialogHide");
            
        }

        hud.OnGameUnPaused();
       // screenParent.SetActive(false);

    }
    private  IEnumerator playAudioCoroutineRestart(AudioSource clip)
    {
        if(hud.isMute == false)
        {
            clip.Play();
            yield return new WaitWhile(() => clip.isPlaying);
        }
        
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
    private IEnumerator playAudioCoroutineMenu(AudioSource clip)
    {
        if(hud.isMute == false)
        {
            clip.Play();
            yield return new WaitWhile(() => clip.isPlaying);
        }
        
        SceneManager.LoadScene("levelSelect");
    }
    public void RestartLevelPress()
    {
        StartCoroutine(playAudioCoroutineRestart(menuButtonClickClip));    
    }


    public void GotoMainMenuPress()
    {
        StartCoroutine(playAudioCoroutineMenu(menuButtonClickClip));        
        
    }
}
