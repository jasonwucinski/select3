﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameOver : MonoBehaviour {

    public GameObject screenParent;
    public GameObject scoreParent;
    public Text gameOverText;
    public Text scoreText;
    public Image[] stars;
    public AudioSource buttonClickClip;
    public HUD hud;

    // Use this for initialization
    void Start()
    {

        screenParent.SetActive(false);
        for (int i = 0; i < stars.Length; i++)
        {
            stars[i].enabled = false;
        }
    }
	// Update is called once per frame
	void Update () {
		
	}

    public void ShowLose()
    {
        screenParent.SetActive(true);
        scoreParent.SetActive(false);
        Animator animator = GetComponent<Animator>();

        if (animator)
        {
            animator.Play("GameOverShow");
        }
    }

    public void ShowWin(int score, int starCount)
    {
        screenParent.SetActive(true);
        gameOverText.enabled = false;
        Animator animator = GetComponent<Animator>();
        scoreText.text = score.ToString();
        scoreText.enabled = false;

        if (animator)
        {
            animator.Play("GameOverShow");
        }
        StartCoroutine(ShowWinCoroutine(starCount));
    }

    private IEnumerator ShowWinCoroutine(int starCount)
    {
        yield return new WaitForSeconds(0.5f);

        if(starCount< stars.Length)
        {
            for(int i = 0; i<= starCount; i++)
            {
                stars[i].enabled = true;
                if(i>0)
                {
                    stars[i - 1].enabled = false;
                }
                yield return new WaitForSeconds(0.5f);
            }
        }
        scoreText.enabled = true;
        gameOverText.enabled = true;
    }

    public void OnReplayClicked()
    {
        StartCoroutine(playAudioCoroutineRestart(buttonClickClip));
    }
    public void OnDoneClicked()
    {
        StartCoroutine(playAudioCoroutineBack(buttonClickClip));
    }

    private IEnumerator playAudioCoroutineBack(AudioSource clip)
    {
        if(hud.isMute == false)
        {
            clip.Play();
            yield return new WaitWhile(() => clip.isPlaying);

        }    
       SceneManager.LoadScene("levelSelect");
    }

    private IEnumerator playAudioCoroutineRestart(AudioSource clip)
    {
        if (hud.isMute == false)
        {
            clip.Play();
            yield return new WaitWhile(() => clip.isPlaying);
        }            
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }


}
