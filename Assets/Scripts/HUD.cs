﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class HUD : MonoBehaviour {
    public Level level;
    public Text remainingText;
    public Text remainingSubText;
    public Text targetText;
    public Text targetSubText;
    public Text scoreText;
    public Text levelText;
    public Image[] stars;
    public GameOver gameOverScreen;
    public WelcomDialog welcomeDialogScreen;
    public SettingsDialog settingsDialogScreen;
    public Text loseText;
    public Text gameOverText;
    public AudioSource star1Clip;
    public AudioSource star2Clip;
    public AudioSource star3Clip;
    private bool played1 = false, played2 = false, played3 = false;
    public bool isMute = false;
    private int startIdx = 0;
    public AudioSource setMadeClip;
    public AudioSource setNotMadeClip;
    // Use this for initialization
    void Start () {
        int m = PlayerPrefs.GetInt("isMute");
        if (m == 0)
        {
            isMute = false;
        }
        else
        {
            isMute = true;
        }
        for (int i =0; i<stars.Length; i++)
        {
            if(i == startIdx)
            {
                stars[i].enabled = true;
            }
            else
            {
                stars[i].enabled = false;
            }
        }

	}
	
	// Update is called once per frame
	void Update () {
        int m = PlayerPrefs.GetInt("isMute");
        if (m == 1)
        {
            isMute = true;
        }
        else
        {
            isMute = false;
        }
    }
    private IEnumerator playAudioCoroutine(AudioSource clip)
    {
        if (isMute == false)
        {
            clip.Play();
            yield return new WaitWhile(() => clip.isPlaying);
        }
            
    }
    public void setLevel(string level)
    {
        level = level.Substring(5);
        levelText.text = level;
    }

    public void SetScore(int score)
    {
        

        scoreText.text = score.ToString();
        int visibleStar = 0;
        if (score >= level.score1Star && score < level.score2Star)
        {
            visibleStar = 1;
            if(played1 == false)
            {
                StartCoroutine(playAudioCoroutine(star1Clip));
                played1 = true;
            }
            
        }
        else if (score >= level.score1Star && score < level.score3Star)
        {
            visibleStar = 2;
            if (played2 == false)
            {
                StartCoroutine(playAudioCoroutine(star2Clip));
                played2 = true;
            }
            else
            {
                if (score > 0)
                {
                    StartCoroutine(playAudioCoroutine(setMadeClip));
                }
            }
               
        }
        else if(score >= level.score3Star)
        {
            visibleStar = 3;
            if (played3 == false)
            {
                StartCoroutine(playAudioCoroutine(star3Clip));
                played3 = true;
            }


        }
        else
        {
            if (score > 0)
            {
                StartCoroutine(playAudioCoroutine(setMadeClip));
            }
        }

        for(int i=0; i<stars.Length; i++)
        {
            if(i == visibleStar)
            {
                stars[i].enabled = true;
                
            }
            else
            {
                stars[i].enabled = false;
              
            }            
        }
        startIdx = visibleStar;
    }

    public void SetGameOverText(string text)
    {      
        gameOverText.text = text;
    }
    public void SetGameLoseText(string text)
    {
        loseText.text = text;
    }

    public void SetTarget(int target)
    {
        targetText.text = target.ToString();
    }
    public void SetRemaining(int remaining)
    {
        remainingText.text = remaining.ToString();
    }
    public void SetRemaingSubText(string remaining)
    {
        remainingSubText.text = remaining;
    }
    public void SetRemaining(string remaining)
    {
        remainingText.text = remaining;
    }
    public void SetTargetSubText(string remaining)
    {
        targetSubText.text = remaining;
    }
    public void SetLevelType(Level.LevelType type)
    {
        if(type == Level.LevelType.TIMER)
        {
            remainingSubText.text = "time remaining";
            targetSubText.text = "target score";
        }
    }
    public void OnGamePaused()
    {
        level.GamePaused();   
        
    }
    public void OnGameUnPaused()
    {
        level.GameUnPaused();

    }
    public void OnGameWin(int score)
    {
        gameOverScreen.ShowWin(score, startIdx);
        if (startIdx > PlayerPrefs.GetInt(SceneManager.GetActiveScene().name, 0))
        {
            PlayerPrefs.SetInt(SceneManager.GetActiveScene().name, startIdx);
        }
    }
    public void OnGameLose()
    {
        gameOverScreen.ShowLose();       
    }

    public void ShowGamePausedMenu()
    {
        level.GamePaused();
        welcomeDialogScreen.ShowWelcomePress("Paused.", "pauseGame");
    }

    public void ShowSettingsDialog()
    {
        level.GamePaused();
        settingsDialogScreen.ShowSettingsPress();
    }
}

