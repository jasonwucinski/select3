﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TilePiece : MonoBehaviour {

    
    public enum TileType
    {
        CIRCLE_BG1_FILL_BLUE,
        CIRCLE_BG1_FILL_GREEN,
        CIRCLE_BG1_FILL_RED,
        CIRCLE_BG1_HOLLOW_BLUE,
        CIRCLE_BG1_HOLLOW_GREEN,
        CIRCLE_BG1_HOLLOW_RED,
        CIRCLE_BG1_STRIPED_BLUE,
        CIRCLE_BG1_STRIPED_GREEN,
        CIRCLE_BG1_STRIPED_RED,
        SQUARE_BG1_FILL_BLUE,
        SQUARE_BG1_FILL_GREEN,
        SQUARE_BG1_FILL_RED,
        SQUARE_BG1_HOLLOW_BLUE,
        SQUARE_BG1_HOLLOW_GREEN,
        SQUARE_BG1_HOLLOW_RED,
        SQUARE_BG1_STRIPED_BLUE,
        SQUARE_BG1_STRIPED_GREEN,
        SQUARE_BG1_STRIPED_RED,
        TRIANGLE_BG1_FILL_BLUE,
        TRIANGLE_BG1_FILL_GREEN,
        TRIANGLE_BG1_FILL_RED,
        TRIANGLE_BG1_HOLLOW_BLUE,
        TRIANGLE_BG1_HOLLOW_GREEN,
        TRIANGLE_BG1_HOLLOW_RED,
        TRIANGLE_BG1_STRIPED_BLUE,
        TRIANGLE_BG1_STRIPED_GREEN,
        TRIANGLE_BG1_STRIPED_RED,
        CIRCLE_BG2_FILL_BLUE,
        CIRCLE_BG2_FILL_GREEN,
        CIRCLE_BG2_FILL_RED,
        CIRCLE_BG2_HOLLOW_BLUE,
        CIRCLE_BG2_HOLLOW_GREEN,
        CIRCLE_BG2_HOLLOW_RED,
        CIRCLE_BG2_STRIPED_BLUE,
        CIRCLE_BG2_STRIPED_GREEN,
        CIRCLE_BG2_STRIPED_RED,
        SQUARE_BG2_FILL_BLUE,
        SQUARE_BG2_FILL_GREEN,
        SQUARE_BG2_FILL_RED,
        SQUARE_BG2_HOLLOW_BLUE,
        SQUARE_BG2_HOLLOW_GREEN,
        SQUARE_BG2_HOLLOW_RED,
        SQUARE_BG2_STRIPED_BLUE,
        SQUARE_BG2_STRIPED_GREEN,
        SQUARE_BG2_STRIPED_RED,
        TRIANGLE_BG2_FILL_BLUE,
        TRIANGLE_BG2_FILL_GREEN,
        TRIANGLE_BG2_FILL_RED,
        TRIANGLE_BG2_HOLLOW_BLUE,
        TRIANGLE_BG2_HOLLOW_GREEN,
        TRIANGLE_BG2_HOLLOW_RED,
        TRIANGLE_BG2_STRIPED_BLUE,
        TRIANGLE_BG2_STRIPED_GREEN,
        TRIANGLE_BG2_STRIPED_RED,
        CIRCLE_BG3_FILL_BLUE,
        CIRCLE_BG3_FILL_GREEN,
        CIRCLE_BG3_FILL_RED,
        CIRCLE_BG3_HOLLOW_BLUE,
        CIRCLE_BG3_HOLLOW_GREEN,
        CIRCLE_BG3_HOLLOW_RED,
        CIRCLE_BG3_STRIPED_BLUE,
        CIRCLE_BG3_STRIPED_GREEN,
        CIRCLE_BG3_STRIPED_RED,
        SQUARE_BG3_FILL_BLUE,
        SQUARE_BG3_FILL_GREEN,
        SQUARE_BG3_FILL_RED,
        SQUARE_BG3_HOLLOW_BLUE,
        SQUARE_BG3_HOLLOW_GREEN,
        SQUARE_BG3_HOLLOW_RED,
        SQUARE_BG3_STRIPED_BLUE,
        SQUARE_BG3_STRIPED_GREEN,
        SQUARE_BG3_STRIPED_RED,
        TRIANGLE_BG3_FILL_BLUE,
        TRIANGLE_BG3_FILL_GREEN,
        TRIANGLE_BG3_FILL_RED,
        TRIANGLE_BG3_HOLLOW_BLUE,
        TRIANGLE_BG3_HOLLOW_GREEN,
        TRIANGLE_BG3_HOLLOW_RED,
        TRIANGLE_BG3_STRIPED_BLUE,
        TRIANGLE_BG3_STRIPED_GREEN,
        TRIANGLE_BG3_STRIPED_RED
    }

    [System.Serializable]
    public struct TileSprite
    {
        public TileType tile;
        public Sprite sprite;
    }
    public TileSprite[] tileSprites;
    private TileType tile;
    private SpriteRenderer sprite;
   

    public TileType tileType
    {
        get { return tile; }
        set { SetTile(value); }
    }
    public int tileSelectNumber;

    private Dictionary<TileType, Sprite> tileSpriteDictionary;

    public int NumTiles
    {
        get { return tileSprites.Length; }        

    }
   
    void Awake()
    {
        sprite = transform.Find("piece").GetComponent<SpriteRenderer>();
        tileSpriteDictionary = new Dictionary<TileType, Sprite>();
        for(int i = 0; i< tileSprites.Length; i++)
        {
            if (!tileSpriteDictionary.ContainsKey(tileSprites[i].tile))
            {
                tileSpriteDictionary.Add(tileSprites[i].tile, tileSprites[i].sprite);
            }
        }


    }

    // Use this for initialization
    void Start () {
        tileSprites = new TileSprite[81];

    }
	
	// Update is called once per frame
	void Update () {
		
	}


    public void SetTile(TileType newTile)
    {
      
        tile = newTile;
        if (tileSpriteDictionary.ContainsKey(newTile))
        {
            sprite.sprite = tileSpriteDictionary[newTile];
        }           
        


    }
}
