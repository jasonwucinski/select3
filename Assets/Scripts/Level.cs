﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.SceneManagement;

public class Level : MonoBehaviour {

    [System.Serializable]
    public enum LevelType
    {
        TIMER,
        OBSTACLE,
        MOVES,
        FREE,
        CLEAR_BOARD,
        TUTORIAL
    }
    public HUD hud;
    protected LevelType type;
    public Grid grid;
    public GamePiece SelectedTile1, SelectedTile2, SelectedTile3;
    public int NumTilesSelected = 0;
    public int NumSetsFound = 0;

    public int NumSameBGFound = 0;
    public int NumSameShapeFound = 0;
    public int NumSameColorFound = 0;
    public int NumSameFillFound = 0;

    public int NumDifferentBGFound = 0;
    public int NumDifferentShapeFound = 0;
    public int NumDifferentColorFound = 0;
    public int NumDifferentFillFound = 0;
    private int levelNumber = 0;


    public WelcomDialog welcomeDialog;
    public LevelType Type
    {
        get { return type; }

    }
    public bool didWin;
   

    public int score1Star, score2Star, score3Star;
    protected int currentScore;


	// Use this for initialization
	 protected void Start () {
        hud.SetScore(0);
        hud.setLevel(SceneManager.GetActiveScene().name);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    public virtual void GamePaused()
    {
        grid.GamePause();
    }
    public virtual void GameUnPaused()
    {
        grid.GameUnPause();
    }

    public virtual void GameWin()
    {    
        grid.GameOver();
        didWin = true;
        StartCoroutine(WaitForGridFill());
 
    }
    public virtual void GameLose()
    {
        
        grid.GameOver();
        didWin = false;
        StartCoroutine(WaitForGridFill());
    }

    protected virtual IEnumerator WaitForGridFill()
    {
        while (grid.isFilling)
        {
            yield return 0;
        }
        if (didWin)
        {
            hud.OnGameWin(currentScore);
        }
        else
        {
            hud.OnGameLose();
        }
    }

    public virtual void OnSelectPiece(GamePiece piece)
    {
        
        if (piece.isSelected == true && NumTilesSelected >= 0)
        {          
            piece.isSelected = false;
            piece.RemoveHighlight();
            RemoveSelectedTile(piece);
            NumTilesSelected--;
            
        }
        else if (piece.isSelected == false && NumTilesSelected <= 3)
        {            
            piece.isSelected = true;
            NumTilesSelected++;

            GetSelectedTile(piece);
            piece.SetHighlight();
            grid.tileSelectClip.Play();
        }

        if (NumTilesSelected == 3)
        {
            if (CheckForSet(SelectedTile1, SelectedTile2, SelectedTile3, true))
            {


                SelectedTile1.clearableComponent.isBeingCleared = true;
                SelectedTile2.clearableComponent.isBeingCleared = true;
                SelectedTile3.clearableComponent.isBeingCleared = true;

                grid.ClearPiece(SelectedTile1.X, SelectedTile1.Y);
                grid.ClearPiece(SelectedTile2.X, SelectedTile2.Y);
                grid.ClearPiece(SelectedTile3.X, SelectedTile3.Y);
                NumSetsFound++;

                if(type != LevelType.CLEAR_BOARD)
                {
                    StartCoroutine(grid.Fill());
                }
                
                OnPieceCleared(piece);

            }

            SelectedTile1.RemoveHighlight();
            SelectedTile2.RemoveHighlight();
            SelectedTile3.RemoveHighlight();

            SelectedTile1.isSelected = false;
            SelectedTile2.isSelected = false;
            SelectedTile3.isSelected = false;

            SelectedTile1 = null;
            SelectedTile2 = null;
            SelectedTile3 = null;

            NumTilesSelected = 0;
        }
    }
    public virtual void OnPieceCleared(GamePiece piece)
    {
        currentScore = currentScore + piece.Score;
        int numMoves = NumberOfSetsRemaining();
        grid.numberMovesLeft = numMoves;
        hud.SetScore(currentScore);   
        if (numMoves == 0)
        {
            GameLose();
        }
        
      
    }
    public static IEnumerable<IEnumerable<T>> GetPermutations<T>(IEnumerable<T> items, int count)
    {
        int i = 0;
        foreach (var item in items)
        {
            if (count == 1)
                yield return new T[] { item };
            else
            {
                foreach (var result in GetPermutations(items.Skip(i + 1), count - 1))
                    yield return new T[] { item }.Concat(result);
            }
            ++i;
        }
    }

    public virtual  int NumberOfSetsRemaining()
    {
        List<List<GamePiece>> NewList = new List<List<GamePiece>>();

        int count = 0;
        List<GamePiece> list = grid.GetConvertArrayToList();
        var result = GetPermutations(list, 3);
        foreach (var perm in result)
        {
            List<GamePiece> p = new List<GamePiece>();
            foreach (var c in perm)
            {
                p.Add(c);
            }
            NewList.Add(p);
        }

        foreach (List<GamePiece> pieces in NewList)
        {
            grid.RemainingMatches = new List<List<GamePiece>>();
            bool isSet = grid.level.CheckForSet(pieces[0], pieces[1], pieces[2], false);
            if (isSet == true)
            {
                if (DoesPieceExist(grid.RemainingMatches, pieces) == false)
                {
                    grid.RemainingMatches.Add(pieces);
                    count++;
                }

            }
        }
        return count;
    }
    public bool DoesPieceExist(List<List<GamePiece>> FullList, List<GamePiece> search)
    {
        for (int i = 0; i < FullList.Count; i++)
        {
            GamePiece item1 = search[0];
            GamePiece item2 = search[1];
            GamePiece item3 = search[2];


            bool alreadyExists1 = FullList[i].Any(p => p.X == item1.X && p.Y == item1.Y);
            bool alreadyExists2 = FullList[i].Any(p => p.X == item2.X && p.Y == item2.Y);
            bool alreadyExists3 = FullList[i].Any(p => p.X == item3.X && p.Y == item3.Y);

            if (alreadyExists1 == true && alreadyExists2 == true && alreadyExists3 == true)
            {
                return true;
            }

        }
        return false;
    }
    private GamePiece GetSelectedTile(GamePiece piece)
    {
        if(SelectedTile1 == null)
        {
            SelectedTile1 = piece;
            piece.selectedTilePosition = 1;
            return SelectedTile1;
        }
        else if (SelectedTile2 == null)
        {
            SelectedTile2 = piece;
            piece.selectedTilePosition = 2;
            return SelectedTile2;
        }
        else
        {
            SelectedTile3 = piece;
            piece.selectedTilePosition = 3;
            return SelectedTile3;
        }   

    }
    private void RemoveSelectedTile(GamePiece piece)
    {
        if(piece.selectedTilePosition == 1)
        {
            SelectedTile1 = null;
        }
        else if(piece.selectedTilePosition == 2)
        {
            SelectedTile2 = null;
        }
        else
        {
            SelectedTile3 = null;
        }


    }
  

    public virtual bool CheckForSet(GamePiece piece1, GamePiece piece2, GamePiece piece3, bool countPieces)
    {
        bool isColorSet;
        bool isFillSet;
        bool isShapeSet;
        bool isBGSet;

        int TempNumSameBGFound = 0;
        int TempNumSameShapeFound = 0;
        int TempNumSameColorFound = 0;
        int TempNumSameFillFound = 0;

        int TempNumDifferentBGFound = 0;
        int TempNumDifferentShapeFound = 0;
        int TempNumDifferentColorFound = 0;
        int TempNumDifferentFillFound = 0;


        if (piece1.TileBG == piece2.TileBG && piece2.TileBG == piece3.TileBG && piece1.TileBG == piece3.TileBG)
        {
            isBGSet = true;
            TempNumSameBGFound++;
        }
        else if (piece1.TileBG != piece2.TileBG && piece2.TileBG != piece3.TileBG && piece1.TileBG != piece3.TileBG)
        {
            isBGSet = true;
            TempNumDifferentBGFound++;
        }
        else
        {
            isBGSet = false;
        }


        if (piece1.TileColor == piece2.TileColor && piece2.TileColor == piece3.TileColor && piece1.TileColor == piece3.TileColor)
        {
            isColorSet = true;
            TempNumSameColorFound++;
        }
        else if (piece1.TileColor != piece2.TileColor && piece2.TileColor != piece3.TileColor && piece1.TileColor != piece3.TileColor)
        {
            isColorSet = true;
            TempNumDifferentColorFound++;
        }
        else
        {
            isColorSet = false;
        }

        if (piece1.TileFill == piece2.TileFill && piece2.TileFill == piece3.TileFill && piece1.TileFill == piece3.TileFill)
        {
            isFillSet = true;
            TempNumSameFillFound++;
        }
        else if (piece1.TileFill != piece2.TileFill && piece2.TileFill != piece3.TileFill && piece1.TileFill != piece3.TileFill)
        {
            isFillSet = true;
            TempNumDifferentFillFound++;
        }
        else
        {
            isFillSet = false;
        }

        if (piece1.TileShape == piece2.TileShape && piece2.TileShape == piece3.TileShape && piece1.TileShape == piece3.TileShape)
        {
            isShapeSet = true;
            TempNumSameShapeFound++;
        }
        else if (piece1.TileShape != piece2.TileShape && piece2.TileShape != piece3.TileShape && piece1.TileShape != piece3.TileShape)
        {
            isShapeSet = true;
            TempNumDifferentShapeFound++;
        }
        else
        {
            isShapeSet = false;
        }

        if (isColorSet == true && isShapeSet == true && isFillSet == true && isBGSet == true)
        {
            if(countPieces == true)
            {
                if (TempNumSameShapeFound > 0)
                {
                    NumSameShapeFound++;
                }
                else
                {
                    NumDifferentShapeFound++;
                }


                if (TempNumSameColorFound > 0)
                {
                    NumSameColorFound++;
                }
                else
                {
                    NumDifferentColorFound++;
                }

                if (TempNumSameFillFound > 0)
                {
                    NumSameFillFound++;
                }
                else
                {
                    NumDifferentFillFound++;
                }

                if (TempNumSameBGFound > 0)
                {
                    NumSameBGFound++;
                }
                else
                {
                    NumDifferentBGFound++;
                }
            }

            
            return true;
        }
        else
        {
            return false;
        }
    }


    //to make harder, enable to make user only pick tiles that are connected
    public bool IsAdjacent()
    {

        if (NumTilesSelected == 1)
        {
            return true;
        }
        else if (NumTilesSelected == 2)
        {
            if (SelectedTile1 != null && SelectedTile2 != null)
            {
                return (SelectedTile1.X == SelectedTile2.X && (int)Mathf.Abs(SelectedTile1.Y - SelectedTile2.Y) == 1)
                || (SelectedTile1.Y == SelectedTile2.Y && (int)Mathf.Abs(SelectedTile1.X - SelectedTile2.X) == 1);
            }
            else if (SelectedTile2 != null && SelectedTile3 != null)
            {
                return (SelectedTile2.X == SelectedTile3.X && (int)Mathf.Abs(SelectedTile2.Y - SelectedTile3.Y) == 1)
                || (SelectedTile2.Y == SelectedTile3.Y && (int)Mathf.Abs(SelectedTile2.X - SelectedTile3.X) == 1);

            }
            else if (SelectedTile1 != null && SelectedTile3 != null)
            {
                return (SelectedTile1.X == SelectedTile3.X && (int)Mathf.Abs(SelectedTile1.Y - SelectedTile3.Y) == 1)
                || (SelectedTile1.Y == SelectedTile3.Y && (int)Mathf.Abs(SelectedTile1.X - SelectedTile3.X) == 1);
            }


        }
        else if (NumTilesSelected == 3)
        {
            if ((SelectedTile1.X == SelectedTile2.X && (int)Mathf.Abs(SelectedTile1.Y - SelectedTile2.Y) == 1)
                || (SelectedTile1.Y == SelectedTile2.Y && (int)Mathf.Abs(SelectedTile1.X - SelectedTile2.X) == 1))
            {
                return true;
            }
            else if ((SelectedTile2.X == SelectedTile3.X && (int)Mathf.Abs(SelectedTile2.Y - SelectedTile3.Y) == 1)
               || (SelectedTile2.Y == SelectedTile3.Y && (int)Mathf.Abs(SelectedTile2.X - SelectedTile3.X) == 1))
            {
                return true;
            }


        }
        return false;

    }
   
}
